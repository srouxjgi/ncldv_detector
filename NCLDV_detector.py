#!/usr/bin/env python
import argparse
import sys
import os
import subprocess
import csv
import re
# import logging
# import fnmatch
# import numpy as np
# import glob
#from Bio import SeqIO
# import pysam
# import pandas
# import concurrent.futures

def load_rbs_types(rbs_file):
	rbs_types = {}
	check_rbs = {}
	with open(rbs_file,'rt') as tsvin:
		tsvin = csv.reader(tsvin, delimiter='\t')
		for row in tsvin:
			rbs_types[row[0]]=row[1]
			check_rbs[row[1]]=1
	tab_rbs=list(check_rbs.keys())
	return (rbs_types,tab_rbs)


def merge(final_file,std_file,custom_file):
	c_seq = ""
	store_custom = {}
	with open(custom_file,'rt') as tsvin:
		tsvin = csv.reader(tsvin, delimiter='\t')
		for row in tsvin:
			if re.match('# Sequence Data:',row[0]):
				c_seq=re.search('seqhdr=\"([^;]+)\"',row[0]).group(1)
				store_custom[c_seq]={}
				print("###### "+c_seq)
				continue
			if re.match('#',row[0]):
				continue
			if row[2] == "CDS":
				stop=row[4]
				if row[6] == "-":
					stop=row[3]
				score=row[5]
				store_custom[c_seq][stop]= {"line":row, "score":score}
				print("for custom - storing "+str(stop)+" with score "+str(score))
	c_seq = ""
	with open(std_file,'rt') as tsvin, open(final_file, 'wt') as csvout:
		tsvin = csv.reader(tsvin, delimiter='\t')
		csvout = csv.writer(csvout, delimiter='\t')
		for row in tsvin:
			if re.match('# Sequence Data:',row[0]):
				if c_seq != "":
					## We add any CDS that would have been unique stops to custom
					for stop in store_custom[c_seq]:
						if "seen" not in store_custom[c_seq][stop]:
							print("We found a unique custom prediction, we add it")
							csvout.writerow(store_custom[c_seq][stop]["line"])
				c_seq=re.search('seqhdr=\"([^;]+)\"',row[0]).group(1)
				csvout.writerow(row)
				continue
			if re.match('#',row[0]):
				csvout.writerow(row)
				continue
			if row[2] == "CDS":
				stop=row[4]
				if row[6] == "-":
					stop=row[3]
				score=float(row[5])
				print("we found a gene with stop "+str(stop)+" and score "+str(score))
				if stop in store_custom[c_seq]:
					store_custom[c_seq][stop]["seen"]=1
					print("for stop "+str(stop)+", we have "+str(score)+" vs custom being "+str(store_custom[c_seq][stop]["score"]))
					if score > float(store_custom[c_seq][stop]["score"]):
						csvout.writerow(row)
					else:
						csvout.writerow(store_custom[c_seq][stop]["line"])
						print("The custom prediction was better !")
				else:
					print("\t and it was not found in custom")
					csvout.writerow(row)
		for stop in store_custom[c_seq]:
			if "seen" not in store_custom[c_seq][stop]:
				print("We found a unique custom prediction, we add it")
				csvout.writerow(store_custom[c_seq][stop]["line"])
	

def load_metrics(in_file,out_file,rbs_type,tab_rbs):
	with open(in_file,'rt') as tsvin, open(out_file, 'wt') as csvout:
		tsvin = csv.reader(tsvin, delimiter='\t')
		csvout = csv.writer(csvout, delimiter=',')
		first_line=["Sequence","Length","Nb_genes","Coding_ratio","Gene_density","Avg_spacer_len"]
		for rbs in tab_rbs:
			first_line.append(rbs)
		csvout.writerow(first_line)
		store_counts = {}
		store_counts_total = {'length':0, 'genes':0, 'coding_bp':0, 'spacers':0, 'None':0}
		previous_seq = ""
		for row in tsvin:
			if re.match('# Sequence Data:',row[0]):
				## Print out the line for the last sequence, and add to the totals
				if previous_seq != "":
					if "genes" not in store_counts:
						store_counts["genes"]=0
						store_counts['coding_bp']=0
					store_counts_total['length']=store_counts_total['length']+store_counts['length']
					store_counts_total['genes']=store_counts_total['genes']+store_counts['genes']
					store_counts_total['coding_bp']=store_counts_total['coding_bp']+store_counts['coding_bp']
					line=[previous_seq,store_counts['length'],store_counts['genes'],store_counts['coding_bp']/store_counts['length'],store_counts['genes']/store_counts['length']*10000]
					if (store_counts['genes']-store_counts['None']) == 0:
						line.append("NA")
					else:
						line.append(store_counts['spacers']/(store_counts['genes']-store_counts['None']))
					for rbs in tab_rbs:
						if rbs not in store_counts:
							store_counts[rbs]=0
						line.append(store_counts[rbs])
					csvout.writerow(line)
				## Prep for next sequence
				length=int(re.search('seqlen=([^;]+)',row[0]).group(1))
				previous_seq=re.search('seqhdr=\"([^;]+)\"',row[0]).group(1)
				previous_seq=re.sub(',','_',previous_seq)
				store_counts = { 'genes':0, 'coding_bp':0, 'length':length, 'spacers':0, 'None':0}
				continue
			if re.match('#',row[0]):
				continue
			if row[2] == "CDS":
				confidence=float(re.search('conf=([^;]+)',row[8]).group(1))
				score=float(re.search('score=([^;]+)',row[8]).group(1))
				if confidence >=90 and score >=50:
					store_counts['genes']+=1
					store_counts['coding_bp']+=abs(int(row[4])-int(row[3]));
					rbs=re.search('rbs_motif=([^;]+)',row[8]).group(1)
					rbs=re.sub('/','_',rbs)
					if rbs in rbs_type:
						rbs=rbs_type[rbs]
					else:
						if re.match('^[GA]+$',rbs):
							print(rbs+" is a other GA")
							rbs="Other_GA"
						else:
							print("No category for rbs "+rbs)
							rbs="Other"
					if rbs not in store_counts:
						store_counts[rbs]=0
					store_counts[rbs]+=1
					if rbs not in store_counts_total:
						store_counts_total[rbs]=0
					store_counts_total[rbs]+=1
					print(row[8]+" ==> "+rbs)
					if rbs != "None":
						spacer=re.search('rbs_spacer=([^;]+)bp',row[8]).group(1)
						if re.search('(\d+)-(\d+)',spacer):
							grouped=re.search('(\d+)-(\d+)',spacer)
							print("UNSURE "+spacer)
							spacer=(int(grouped.group(1))+int(grouped.group(2)))/2
						store_counts['spacers']+=float(spacer)
						store_counts_total['spacers']+=float(spacer)
						print("Final "+row[8]+" ==> "+str(spacer))
		## Don't forget the last sequence
		if previous_seq != "":
			store_counts_total['length']=store_counts_total['length']+store_counts['length']
			store_counts_total['genes']=store_counts_total['genes']+store_counts['genes']
			store_counts_total['coding_bp']=store_counts_total['coding_bp']+store_counts['coding_bp']
			line=[previous_seq,store_counts['length'],store_counts['genes'],store_counts['coding_bp']/store_counts['length'],store_counts['genes']/store_counts['length']*10000]
			if (store_counts['genes']-store_counts['None']) == 0:
				line.append("NA")
			else:
				line.append(store_counts['spacers']/(store_counts['genes']-store_counts['None']))
			for rbs in tab_rbs:
				if rbs not in store_counts:
					store_counts[rbs]=0
				line.append(store_counts[rbs])
			csvout.writerow(line)
		## And also make a line to get the global stats on the whole file (meaningless if metaG, but useful if draft genome or MAG)
		line=["Global",store_counts_total['length'],store_counts_total['genes'],store_counts_total['coding_bp']/store_counts_total['length'],store_counts_total['genes']/store_counts_total['length']*10000]
		if (store_counts_total['genes']-store_counts_total['None']) == 0:
			line.append("NA")
		else:
			line.append(store_counts_total['spacers']/(store_counts_total['genes']-store_counts_total['None']))
		for rbs in tab_rbs:
			if rbs not in store_counts_total:
				store_counts_total[rbs]=0
			line.append(store_counts_total[rbs])
		csvout.writerow(line)


def summarize(in_file,out_file,seq_type,mode):
	with open(in_file,'rt') as tsvin, open(out_file, 'wt') as csvout:
		tsvin = csv.reader(tsvin, delimiter='\t')
		csvout = csv.writer(csvout, delimiter=',')
		translate = {}
		header = next(tsvin, None)
		csvout.writerow(["## NCLDV_detector.py: sequence type = "+str(seq_type)+" / progidal mode = "+str(mode)])
		csvout.writerow(["## Sequence","N genes","Ratio TATATA_3.6","Pred_simple_NCLDV_score", "Pred_complex_NCLDV_score", "Pred_complex_best", "Pred_complex_best_score"])
		for row in tsvin:
			seq = row[1]
			n_genes = str(row[3])
			ratio_tata = row[12]
			pred_simple = row[18]
			pred_complex = row[26]
			sc_max = 0
			best = "NA"
			for i in range(19,28):
				if row[i] != "NA":
					row[i] = float(row[i])
					#print("comparing "+str(row[i])+" vs "+str(sc_max))
					if row[i] > sc_max:
						sc_max = row[i]
						best = header[i-1] ## Headers are shifted one cell to the left because R .. 
			csvout.writerow([seq,n_genes,ratio_tata,pred_simple,pred_complex,best,sc_max])


def main():
	parser = argparse.ArgumentParser(description='Try to predict NCLDV sequences based on gene density, size, and predicted RBS.')
	parser.add_argument('--fa_file','-f', dest='fa_file', required=False, default='none',
				   help='input fasta file')
	parser.add_argument('--gff_file','-g', dest='gff_file', required=False, default='none',
				   help='result from Prodigal')
	parser.add_argument('--mode','-m', dest='ptype', required=False, default='regular',
				   help='Mode of RBS prediction in prodigal: default = regular, can be changed to "fullmotif". Note: you can\'t use the fullmotif mode with metagenomes')
	parser.add_argument('--type','-t', dest='stype', required=False, default='single',
				   help='Single or multiple genomes (i.e. metagenome contigs) in the input file: default = single, can be changed to "multiple". Multiple means that there will not be a training based on the input file, since the sequences are not from a single genome. Instead, predictions will be done with the default training set, then with a custom NCLDV (without Pandoravirus) training set, and predictions are merged based on best score. The single mode instead allows for the (potential) identification of new motifs from the input sequences.')
	parser.add_argument('--out','-o', dest='outdir', required=False, default='',
				   help='output directory (if not provided, output will be written in the same directory where the input file is)')
	### ABSOLUTE PATH FOR REF FILES
	rbs_file='/global/dna/projectdirs/MEP/tools/NCLDV_detector/Classify_RBS.tsv'
	training_file='/global/dna/projectdirs/MEP/tools/NCLDV_detector/NCLDV_db.tn'
	r_script_path='/global/dna/projectdirs/MEP/tools/NCLDV_detector/run_predict_models.R'
	### NOTE THAT WE ALSO NEED THE FILE Models.RData IN THE SAME  /global/dna/projectdirs/MEP/tools/NCLDV_detector/ folder
	### END OF ABSOLUTE PATH FOR REF FILES
	global args
	args = parser.parse_args()
	fasta_file=args.fa_file
	gff_file=args.gff_file
	prodigal_type=args.ptype
	seq_type=args.stype
	## Prepping the path for the out file
	base=str(os.path.splitext(fasta_file)[0])
	if fasta_file == 'none':
		base=print(os.path.splitext(gff_file)[0])
	if (fasta_file == 'none' and gff_file == 'none') or (fasta_file != 'none' and gff_file != 'none'):
		print("please provide either a fasta file or gff file as input")
		sys.exit()
	## switch output dir if needed
	if args.outdir != '':
		filename=str(os.path.basename(base))
		base=args.outdir+"/"+filename
	## Loading RBS info
	(rbs_type, tab_rbs) = load_rbs_types(rbs_file)
	csv_file = base+"_metrics.csv"
	predict_outfile = base+"_predicted.tsv"
	final_outfile = base+"_predict_summarized.csv"
	# First run prodigal if needed
	if seq_type == 'single' :
		if gff_file == 'none': 
			gff_file=base+"_prodigal"+prodigal_type+"_single.gff"
			if prodigal_type == 'regular' :
				bashCommand = "prodigal -f gff -i "+fasta_file+" -o "+gff_file
				print(bashCommand)
				process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
				process.wait()
			else:
				bashCommand = "prodigal -f gff -n -i "+fasta_file+" -o "+gff_file
				print(bashCommand)
				process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
				process.wait()
	
	elif seq_type == 'multiple' :
		if gff_file == 'none':
			gff_file_std=base+"_prodigal"+prodigal_type+"_std.gff"
			gff_file_custom=base+"_prodigal"+prodigal_type+"_customtraining.gff"
			gff_file=base+"_prodigal"+prodigal_type+"_merged.gff"
			if prodigal_type == 'regular' :
				bashCommand = "prodigal -f gff -p meta -i "+fasta_file+" -o "+gff_file_std
				print(bashCommand)
				process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
				process.wait()
				bashCommand = "prodigal -f gff -t "+training_file+" -i "+fasta_file+" -o "+gff_file_custom
				print(bashCommand)
				process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
				process.wait()
			else:
				sys.exit("You probably don't want to actually try fullmotif on a multiple mode")
				#bashCommand = "prodigal -f gff -p meta -n -i "+fasta_file+" -o "+gff_file_std
				#print(bashCommand)
				#process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
				#process.wait()
				#bashCommand = "prodigal -f gff -t "+training_file+" -i "+fasta_file+" -o "+gff_file_custom
				#print(bashCommand)
				#process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
				#process.wait()
			merge(gff_file,gff_file_std,gff_file_custom)
	
	else:
		sys.exit("I don't understand the type "+seq_type+" -- should be single or multiple")
	##input("Press Enter to continue...")
	# Then regardless of how we generated the gff file, we prep the input file for R script
	load_metrics(gff_file,csv_file,rbs_type,tab_rbs)
	
	# Then run the R script on the previously generated file
	bashCommand = "Rscript "+r_script_path+" "+csv_file+" "+predict_outfile+" "+prodigal_type
	if seq_type == 'multiple':
		bashCommand = "Rscript "+r_script_path+" "+csv_file+" "+predict_outfile+" custom"
	print(bashCommand)
	process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
	process.wait()
			
	# And create a summarized table of predictions
	## 4 columns: Sequence ID , Pred_simple_NCLDV_score, Pred_complex_NCLDV_score, Pred_complex_best, Pred_complex_best_score
	summarize(predict_outfile, final_outfile, seq_type, prodigal_type)


if __name__ == "__main__":
	output = main()
