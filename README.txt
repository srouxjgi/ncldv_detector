### How to run NCLDV detector on a metagenome assembly (i.e. set of unrelated contigs)
source activate NCLDV_detector
/global/dna/projectdirs/MEP/tools/NCLDV_detector/NCLDV_detector.py --fa_file your_fasta_file --mode regular --type multiple --out your_output_directory
###
###
### How to run NCLDV detector on a MAG / SAG / draft genome (i.e. set of contigs that should be all the same genome)
source activate NCLDV_detector
/global/dna/projectdirs/MEP/tools/NCLDV_detector/NCLDV_detector.py --fa_file your_fasta_file --mode fullmotif --type single --out your_output_directory


